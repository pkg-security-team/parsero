parsero (0.0+git20140929.e5b585a-7) unstable; urgency=medium

  * Team Upload
  * Fix SyntaxWarning (Closes: #1085770)
  * Remove depedency on python3-pkg-resources (Closes: #1083526)

 -- Alexandre Detiste <tchet@debian.org>  Sun, 09 Feb 2025 11:52:15 +0100

parsero (0.0+git20140929.e5b585a-6) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/copyright: updated packaging copyright years.
  * debian/source/lintian-overrides: updated and fixed a typo.

 -- Thiago Andrade Marques <andrade@debian.org>  Sun, 22 Jan 2023 20:37:58 -0300

parsero (0.0+git20140929.e5b585a-5) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 4.6.0.
      - Removed python3-pip in Depends field. (Closes: #984444)
  * debian/copyright: updated packaging copyright years.
  * debian/patches/10_remove-pip-from-install-requires.patch: added.
  * debian/patches/series: added.

 -- Thiago Andrade Marques <andrade@debian.org>  Thu, 23 Sep 2021 07:59:32 -0300

parsero (0.0+git20140929.e5b585a-4) unstable; urgency=medium

  [ Thiago Andrade Marques]
  * debian/control:
      - Bumped "debhelper-compat" from 12 to 13.
      - Updated my email address.
  * debian/copyright:
      - Added rights to Samuel Henrique.
      - Updated my email address.
  * debian/manpage/*:
      - Updated my email address.
      - Updated the date.
  * debian/source/lintian-overrides: created to override a message about
    manpage, as requested by lintian.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Thiago Andrade Marques <andrade@debian.org>  Tue, 18 Aug 2020 16:14:43 -0300

parsero (0.0+git20140929.e5b585a-3) unstable; urgency=medium

  * debian/control: added python3-pkg-resources in the Depends field.
  * debian/salsa-ci.yml: removed allow_failure to autopkgtest,
      no loonger needed.

 -- Thiago Andrade Marques <thmarques@gmail.com>  Fri, 21 Feb 2020 14:14:06 -0300

parsero (0.0+git20140929.e5b585a-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/salsa-ci.yml: added allow_failure to autopkgtest.

 -- Thiago Andrade Marques <thmarques@gmail.com>  Thu, 13 Feb 2020 23:51:57 -0300

parsero (0.0+git20140929.e5b585a-1) experimental; urgency=medium

  * Initial release (Closes: #949198)

 -- Thiago Andrade Marques <thmarques@gmail.com>  Mon, 27 Jan 2020 17:11:23 -0300
